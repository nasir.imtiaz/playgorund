# Playground 

A java based library for Playground management.

### Features
The application provides following features:
  - Play site registration
  - Registering kids to play site
  - Removing kids from play site
  - Dequeue kids from play site
  - Generate periodic play site utilization snapshots
  - Generate usage history for all registered play sites
  - Generate visitor history for all registered play sites

### How to use:
  - Make a reference to 'org.swedbank.some.system.service.PlayGroundService' in order to utilize following play ground features:
	- `registerPlaySite(PlaySite)` - To register a given play site.
	- `addKidToPlaySite(Kid, PlaySite)` - To register a kid to a play site.
	- `removeKidFromPlaySite(Kid, PlaySite)` - To remove a kid from a play site.
	- `dequeueKidFromPlaySite(Kid, PlaySite)` - To dequeue a kid from a play site.
	- `getAllPlaySiteUtilizationSnapshots()` - To generate periodic play site utilization snapshots
	- `getAllPlaySiteUsageHistory()` - To generate usage history for all registered play sites
	- `getAllPlaySiteVisitHistory()` - To generate visitor history for all registered play sites

### How to run:
  - clone this project into your local directory: 
  `git clone https://gitlab.com/nasir.imtiaz/playgorund.git`
  - Build the project: 
  `mvn clean install`
  - Include the generated `playground-1.0-SNAPSHOT.jar` jar into your project
