package org.swedbank.some.system.domain;

import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import org.swedbank.some.system.common.PlayGroundUtility;
import org.swedbank.some.system.exception.InvalidPeriodException;
import org.swedbank.some.system.exception.KidAlreadyOnPlaySiteException;
import org.swedbank.some.system.exception.KidNotFoundException;
import org.swedbank.some.system.exception.PlaySiteAlreadyRegisteredException;
import org.swedbank.some.system.exception.PlaySiteNotRegisteredException;

public class DomainServiceImpl implements DomainService {
	private List<PlaySite> registeredPlaySites;
	private HashMap<PlaySite, List<Snapshot>> playSiteSnapshots;
	private LocalTime workingHourstartTime;
	private LocalTime workingHourEndTime;
	
	public DomainServiceImpl (LocalTime workingHourstartTime, LocalTime workingHourEndTime) {
		verifyWorkingHourPeriod(workingHourstartTime, workingHourEndTime);
		
		this.workingHourstartTime = workingHourstartTime;
		this.workingHourEndTime = workingHourEndTime;
	}
	
	private void verifyWorkingHourPeriod(LocalTime workingHourstartTime, LocalTime workingHourEndTime) {
		if (workingHourstartTime == null || workingHourEndTime == null || workingHourEndTime.isBefore(workingHourstartTime)) {
			throw new InvalidPeriodException("Please specify a valid period for working hours.");
		}
	}
	
	@Override
	public void registerPlaySite(PlaySite playSite) {
		if (registeredPlaySites == null) {
			registeredPlaySites = new ArrayList<>();
			playSiteSnapshots = new HashMap<>();
		}

		validatePlaySiteAlreadyRegistered(playSite);

		registeredPlaySites.add(playSite);

		registerPlaySiteUtilizationTask(playSite);
	}
	
	private void validatePlaySiteAlreadyRegistered(PlaySite playSite) {
		if (isPlaySiteRegistered(playSite)) {
			throw new PlaySiteAlreadyRegisteredException(
					"Given play site '" + playSite.getName() + "' is already registered.");
		}
	}

	private boolean isPlaySiteRegistered(PlaySite playSite) {
		return registeredPlaySites.contains(playSite);
	}
	
	private void registerPlaySiteUtilizationTask(PlaySite playSite) {
		Timer time = new Timer();

		time.schedule(new TimerTask() {
			@Override
			public void run() {
				if (PlayGroundUtility.isWorkingHour(workingHourstartTime, workingHourEndTime)) {
					recordSnapshot(playSite);
				}
			}
		}, 0, playSite.getSnapshotIntervalInSec());
	}

	private void recordSnapshot(PlaySite playSite) {
		List<Snapshot> snapshots = playSiteSnapshots.getOrDefault(playSite, new ArrayList<>());
		snapshots.add(new Snapshot(playSite.utilization(), LocalDateTime.now()));
		playSiteSnapshots.put(playSite, snapshots);
	}

	@Override
	public synchronized int addKidToPlaySite(Kid kid, PlaySite playSite) {
		if (! isPlaySiteRegistered(playSite)) {
			registerPlaySite(playSite);
		}
		
		validateKidAlreadyOnPlaySite(kid);

		return playSite.addKid(kid);
	}
	
	private void validateKidAlreadyOnPlaySite(Kid kid) {

		if (registeredPlaySites != null && !registeredPlaySites.isEmpty()) {

			for (PlaySite playSite : registeredPlaySites) {
				if (playSite.isKidPresent(kid)) {
					throw new KidAlreadyOnPlaySiteException(
							"Given kid '" + kid.getName() + "' already on a play site '" + playSite.getName() + "'.");
				}
			}
		}
	}
	
	@Override
	public synchronized void removeKidFromPlaySite(Kid kid, PlaySite playSite) {
		validateKidRegisteredWithPlaySite(playSite, kid);

		playSite.removeKid(kid);
	}
	
	private void validateKidRegisteredWithPlaySite(PlaySite playSite, Kid kid) {
		validateIsPlaySiteRegistered(playSite);

		if (!playSite.isKidPresent(kid)) {
			throw new KidNotFoundException(
					"Given kid '" + kid.getName() + "' not found at given play site '" + playSite.getName() + "'.");
		}
	}

	private void validateIsPlaySiteRegistered(PlaySite playSite) {
		if (! isPlaySiteRegistered(playSite)) {
			throw new PlaySiteNotRegisteredException("Given play site '" + playSite.getName()
					+ "' is not registered. Please register the play site first in order to perform the required operation.");
		}
	}
	
	@Override
	public synchronized void dequeueKidFromPlaySite(Kid kid, PlaySite playSite) {
		validateKidEnqueuedWithPlaySite(playSite, kid);

		playSite.dequeueKid(kid);
	}

	private void validateKidEnqueuedWithPlaySite(PlaySite playSite, Kid kid) {
		validateIsPlaySiteRegistered(playSite);

		if (!playSite.isKidEnqueued(kid)) {
			throw new KidNotFoundException(
					"Given kid '" + kid.getName() + "' not queued at given play site '" + playSite.getName() + "'.");
		}
	}
	
	@Override
	public HashMap<PlaySite, List<Snapshot>> getAllPlaySiteUtilizationSnapshots() {
		return playSiteSnapshots;
	}

	@Override
	public List<PlaySiteUsageHistory> getAllPlaySiteUsageHistory() {
		List<PlaySiteUsageHistory> history = null;

		if (PlayGroundUtility.isNotEmptyList(registeredPlaySites)) {
			history = new ArrayList<>();

			for (PlaySite playSite : registeredPlaySites) {
				if (playSite.getTotalPlayDurationInSec() > 0) {
					history.add(new PlaySiteUsageHistory(playSite, playSite.getTotalPlayDurationInSec()));
				}
			}
		}

		return history;
	}

	@Override
	public List<PlaySiteVisitHistory> getAllPlaySiteVisitHistory() {
		List<PlaySiteVisitHistory> history = null;

		if (PlayGroundUtility.isNotEmptyList(registeredPlaySites)) {
			history = new ArrayList<>();

			for (PlaySite playSite : registeredPlaySites) {
				history.add(new PlaySiteVisitHistory(playSite, playSite.getTotalVisitorCount()));
			}
		}

		return history;
	}
}
