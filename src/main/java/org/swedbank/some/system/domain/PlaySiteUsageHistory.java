package org.swedbank.some.system.domain;

public class PlaySiteUsageHistory {
	private PlaySite playSite;
	private long totalPlayDurationInSec;
	
	private PlaySiteUsageHistory() {
		// TODO Auto-generated constructor stub
	}

	public PlaySiteUsageHistory(PlaySite playSite, long totalPlayDurationInSec) {
		super();
		this.playSite = playSite;
		this.totalPlayDurationInSec = totalPlayDurationInSec;
	}

	public PlaySite getPlaySite() {
		return playSite;
	}

	public long getTotalPlayDurationInSec() {
		return totalPlayDurationInSec;
	}

}
