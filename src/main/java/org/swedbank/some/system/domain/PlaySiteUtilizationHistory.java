package org.swedbank.some.system.domain;

import java.time.LocalDateTime;
import java.util.List;

public class PlaySiteUtilizationHistory {
	private PlaySite playSite;
	private List<Snapshot> snapshots;
	
	private PlaySiteUtilizationHistory() {
		// TODO Auto-generated constructor stub
	}

	public PlaySiteUtilizationHistory(PlaySite playSite, double utilization, LocalDateTime timeStamp) {
		super();
		this.playSite = playSite;
	}

	public PlaySite getPlaySite() {
		return playSite;
	}

	public List<Snapshot> getSnapshots() {
		return snapshots;
	}

}
