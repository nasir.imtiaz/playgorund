package org.swedbank.some.system.domain;

import java.util.HashMap;
import java.util.List;

public interface DomainService {
	/**
	 * Registers a given play site.
	 * @param playSite play site to be registered
	 * @throws PlaySiteAlreadyRegisteredException - if the given play site is already registered
	 */
	public void registerPlaySite(PlaySite playSite);

	/**
	 * Registers given kid to the specified play site. If play site is not registered then it is internally registered first.
	 * If the play site utilization is full and the kid is willing to wait then the kid is enqueued.
	 * @param kid kid to register for a given play site
	 * @param playSite play site to register given kid to
	 * @return In case kid is not willing to wait then -1 is returned otherwise 1 is returned
	 * @throws KidAlreadyOnPlaySiteException - if the given kid is already registered with an existing play site
	 */
	public int addKidToPlaySite(Kid kid, PlaySite playSite);

	/**
	 * Removes given kid from given play site. After removing the given kid, if the play site has a queue then the next in queue is removed from the queue and sent to play.
	 * If the given kid is not playing on the play site but enqueued then it is dequeued. 
	 * @param kid kid to remove for a given play site
	 * @param playSite play site to remove given kid from
	 * @throws PlaySiteNotRegisteredException - if the given play site is not registered
	 * @throws KidNotFoundException - if the given kid is not found on the specified play site
	 */
	public void removeKidFromPlaySite(Kid kid, PlaySite playSite);

	/**
	 * Dequeues given kid from the specified play site.
	 * @param kid kid to dequeue for a given play site
	 * @param playSite play site to dequeue given kid from
	 * @throws PlaySiteNotRegisteredException - if the given play site is not registered
	 * @throws KidNotFoundException - if the given kid is not found in the queue of specified play site
	 */
	public void dequeueKidFromPlaySite(Kid kid, PlaySite playSite);

	/**
	 * Returns play site utilization snapshots for all registered play sites during working hours.
	 * @return collection of play site utilization snapshots for all registered play sites during working hours.
	 */
	public HashMap<PlaySite, List<Snapshot>> getAllPlaySiteUtilizationSnapshots();

	/**
	 * Returns usage history for all registered play sites.
	 * @return collection of usage history for all registered play sites.
	 */
	public List<PlaySiteUsageHistory> getAllPlaySiteUsageHistory();
	
	/**
	 * Returns visitor history for all registered play sites.
	 * @return collection of visitor history for all registered play sites.
	 */
	public List<PlaySiteVisitHistory> getAllPlaySiteVisitHistory();
}
