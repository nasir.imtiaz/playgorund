package org.swedbank.some.system.domain;

import java.time.LocalDateTime;

public class Snapshot {
	private double utilization;
	private LocalDateTime timeStamp;

	private Snapshot() {
		// TODO Auto-generated constructor stub
	}

	public Snapshot(double utilization, LocalDateTime timeStamp) {
		super();
		this.utilization = utilization;
		this.timeStamp = timeStamp;
	}

	public double getUtilization() {
		return utilization;
	}

	public LocalDateTime getTimeStamp() {
		return timeStamp;
	}

}
