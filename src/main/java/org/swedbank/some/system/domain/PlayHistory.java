package org.swedbank.some.system.domain;

import java.time.LocalDateTime;

public class PlayHistory {
	private LocalDateTime playStartTime;
	private LocalDateTime playEndTime;
	private int kidCount;
	
	public PlayHistory() {
		
	}

	public LocalDateTime getPlayStartTime() {
		return playStartTime;
	}

	public void setPlayStartTime(LocalDateTime playStartTime) {
		this.playStartTime = playStartTime;
	}

	public LocalDateTime getPlayEndTime() {
		return playEndTime;
	}

	public void setPlayEndTime(LocalDateTime playEndTime) {
		this.playEndTime = playEndTime;
	}

	public int getKidCount() {
		return kidCount;
	}

	public void incrementKidCount() {
		this.kidCount++;
	}

}
