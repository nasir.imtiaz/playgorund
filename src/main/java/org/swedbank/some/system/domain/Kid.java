package org.swedbank.some.system.domain;

public class Kid {
	private String name;
	private int age;
	private String ticketNumber;
	private boolean acceptWait;
	private boolean vip;

	public Kid(String name, int age, String ticketNumber, boolean acceptWait, boolean vip) {
		super();
		this.name = name;
		this.age = age;
		this.ticketNumber = ticketNumber;
		this.acceptWait = acceptWait;
		this.vip = vip;
	}

	public Kid(String name, int age, String ticketNumber, boolean acceptWait) {
		super();
		this.name = name;
		this.age = age;
		this.ticketNumber = ticketNumber;
		this.acceptWait = acceptWait;
	}
	
	public Kid(String name, int age, String ticketNumber) {
		super();
		this.name = name;
		this.age = age;
		this.ticketNumber = ticketNumber;
	}

	public String getName() {
		return name;
	}

	public int getAge() {
		return age;
	}

	public String getTicketNumber() {
		return ticketNumber;
	}

	public boolean isAcceptWait() {
		return acceptWait;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((ticketNumber == null) ? 0 : ticketNumber.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Kid other = (Kid) obj;
		if (ticketNumber == null) {
			if (other.ticketNumber != null)
				return false;
		} else if (!ticketNumber.equals(other.ticketNumber))
			return false;
		return true;
	}

	public boolean isVip() {
		return vip;
	}

}
