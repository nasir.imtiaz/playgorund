package org.swedbank.some.system.domain;

public class PlaySiteVisitHistory {
	private PlaySite playSite;
	private long visitCount;
	
	private PlaySiteVisitHistory() {
		// TODO Auto-generated constructor stub
	}

	public PlaySiteVisitHistory(PlaySite playSite, long visitCount) {
		super();
		this.playSite = playSite;
		this.visitCount = visitCount;
	}

	public PlaySite getPlaySite() {
		return playSite;
	}

	public long getVisitCount() {
		return visitCount;
	}

}
