package org.swedbank.some.system.domain;

import static org.swedbank.some.system.common.Constants.EMPTY_UTILIZATION;
import static org.swedbank.some.system.common.Constants.FULL_UTILIZATION;

public class DoubleSwing extends PlaySite {

	public DoubleSwing(String name, int snapshotIntervalInSec) {
		super(name, 2, snapshotIntervalInSec); // no need to take capacity from caller as it is known
	}

	@Override
	public double utilization() {
		if (this.getPlayingKidCount() == this.getCapacity())
			return FULL_UTILIZATION;
		else
			return EMPTY_UTILIZATION;
	}

}
