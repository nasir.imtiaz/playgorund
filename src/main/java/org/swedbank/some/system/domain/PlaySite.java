package org.swedbank.some.system.domain;

import static org.swedbank.some.system.common.Constants.FULL_UTILIZATION;
import static org.swedbank.some.system.common.Constants.NO_KID_PLAYING;
import static org.swedbank.some.system.common.Constants.NO_WAITING;
import static org.swedbank.some.system.common.Constants.SUCCESS;
import static org.swedbank.some.system.common.Constants.VIP_JUMP_FACTOR;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.swedbank.some.system.common.PlayGroundUtility;
import org.swedbank.some.system.exception.InvalidPlaySiteCapacityException;

public abstract class PlaySite {
	private String name;
	private int capacity;
	private List<Kid> playingKids;
	private List<Kid> queuedKids;
	private int snapshotIntervalInSec;
	private LocalDateTime playStartedAt;
	private long totalPlayDurationInSec;
	private long totalVisitorCount;

	private PlaySite() {

	}

	public PlaySite(String name, int capacity, int snapshotIntervalInSec) {
		this.name = name;

		if (capacity < 1) {
			throw new InvalidPlaySiteCapacityException("Invalid capacity '" + capacity + "', should be at least 1.");
		}

		this.capacity = capacity;
		this.snapshotIntervalInSec = snapshotIntervalInSec;
		playingKids = new ArrayList<>(capacity);
		queuedKids = new ArrayList<>();
	}

	protected double utilization() {
		return ((getPlayingKidCount() / (double) capacity) * 100);
	}

	public int getCapacity() {
		return capacity;
	}

	protected int getPlayingKidCount() {
		return playingKids.size();
	}

	int addKid(Kid kid) {

		if (utilization() == FULL_UTILIZATION) {
			return enqueueKid(kid);
		}

		// method to maintain play duration and visitor count for this play site
		onBeforeAddKid();

		playingKids.add(kid);

		return SUCCESS;
	}

	private int enqueueKid(Kid kid) {
		// Checking if kid accepts waiting in queue
		if (!kid.isAcceptWait()) {
			return NO_WAITING;
		}

		// Checking if kid has VIP ticket
		if (kid.isVip()) {
			int nexVipPlacementPosition = getNextVipPlacementPosition();

			queuedKids.add(nexVipPlacementPosition, kid);
		} else
			queuedKids.add(kid);

		totalVisitorCount++;

		return SUCCESS;
	}

	private int getNextVipPlacementPosition() {
		int nexVipPlacementPosition = 0;

		Iterator<Kid> iterator = queuedKids.iterator();

		while (iterator.hasNext()) {
			Kid currentKid = iterator.next();
			if (currentKid.isVip()) {
				int nonVipKidCount = 0;
				nexVipPlacementPosition++;

				while (iterator.hasNext() && nonVipKidCount < VIP_JUMP_FACTOR) {
					currentKid = iterator.next();
					if (!currentKid.isVip())
						nonVipKidCount++;

					nexVipPlacementPosition++;
				}
			} else
				break;
		}

		return nexVipPlacementPosition;
	}

	void removeKid(Kid kid) {
		boolean kidRemoved = playingKids.remove(kid); 
		
		if (kidRemoved) {
			// In case there was a queue then, the next in queue is dequeued and put to play
			dequeueKidToPlay();
			
			// method to maintain play duration for this play site
			onAfterRemoveKid();
		} else {
			// There is a chance that the kid might have been queued and not started playing yet
			if (queuedKids.contains(kid)) {
				queuedKids.remove(kid);
			}
		}
	}

	private void dequeueKidToPlay() {
		if (queuedKids.isEmpty()) {
			return;
		}

		Kid kid = queuedKids.remove(0);
		playingKids.add(kid);
	}

	void dequeueKid(Kid kid) {
		queuedKids.remove(kid);
	}

	public int getSnapshotIntervalInSec() {
		return snapshotIntervalInSec;
	}

	public String getName() {
		return name;
	}

	private void onBeforeAddKid() {
		if (getPlayingKidCount() == NO_KID_PLAYING) {
			playStartedAt = LocalDateTime.now();
		}

		totalVisitorCount++;
	}

	private void onAfterRemoveKid() {
		if (getPlayingKidCount() == NO_KID_PLAYING) {
			totalPlayDurationInSec += PlayGroundUtility.getDuration(playStartedAt, LocalDateTime.now(),
					ChronoUnit.SECONDS);
			playStartedAt = null;
		}
	}

	private boolean isAnyKidPlaying() {
		return getPlayingKidCount() > NO_KID_PLAYING;
	}

	long getTotalPlayDurationInSec() {
		// In case a kid is playing while a request comes to generate report of how long kids have played on a play site,
		// then the time till now will also be considered in the period.
		if (isAnyKidPlaying()) {
			totalPlayDurationInSec += PlayGroundUtility.getDuration(playStartedAt, LocalDateTime.now(),
					ChronoUnit.SECONDS);
		}

		return totalPlayDurationInSec;
	}

	boolean isKidPresent(Kid kid) {
		return playingKids.contains(kid) || queuedKids.contains(kid);
	}

	boolean isKidEnqueued(Kid kid) {
		return queuedKids.contains(kid);
	}

	long getTotalVisitorCount() {
		return totalVisitorCount;
	}

}