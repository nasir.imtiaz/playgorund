package org.swedbank.some.system.common;

import java.time.Duration;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.temporal.ChronoUnit;
import java.util.List;

public class PlayGroundUtility {

	private PlayGroundUtility() {
		// TODO Auto-generated constructor stub
	}
	
	public static boolean isWorkingHour(LocalTime startTime, LocalTime endTime) {
		return ( ( LocalTime.now().equals(startTime) || LocalTime.now().isAfter(startTime) ) && ( LocalTime.now().isBefore(endTime) ) );
	}
	
	public static long getDuration(LocalDateTime start, LocalDateTime end, ChronoUnit unit) {
		if (! start.isAfter(end)) {
			Duration duration = Duration.between(start, end);
			return duration.get(unit);
		}
		
		return -1;
	}

	public static boolean isEmptyList(List list) {
		return (list == null || list.isEmpty());
	}
	
	public static boolean isNotEmptyList(List list) {
		return ! isEmptyList(list);
	}
	
}
