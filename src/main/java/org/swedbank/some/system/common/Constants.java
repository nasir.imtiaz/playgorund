package org.swedbank.some.system.common;

public class Constants {
	public static final double FULL_UTILIZATION = 100;
	public static final double EMPTY_UTILIZATION = 0;
	public static final int NO_KID_PLAYING = 0;
	public static final int NO_WAITING = -1;
	public static final int VIP_JUMP_FACTOR = 3;
	public static final int SUCCESS = 1;
}
