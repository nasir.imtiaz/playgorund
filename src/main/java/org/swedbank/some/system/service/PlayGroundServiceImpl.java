package org.swedbank.some.system.service;

import java.time.LocalTime;
import java.util.HashMap;
import java.util.List;

import org.swedbank.some.system.domain.DomainService;
import org.swedbank.some.system.domain.DomainServiceImpl;
import org.swedbank.some.system.domain.Kid;
import org.swedbank.some.system.domain.PlaySite;
import org.swedbank.some.system.domain.PlaySiteUsageHistory;
import org.swedbank.some.system.domain.PlaySiteVisitHistory;
import org.swedbank.some.system.domain.Snapshot;

public class PlayGroundServiceImpl implements PlayGroundService {
	private DomainService domainService;
	
	public PlayGroundServiceImpl(LocalTime workingHourstartTime, LocalTime workingHourEndTime) {
		domainService = new DomainServiceImpl(workingHourstartTime, workingHourEndTime);
	}

	@Override
	public void registerPlaySite(PlaySite playSite) {
		domainService.registerPlaySite(playSite);
	}

	@Override
	public int addKidToPlaySite(Kid kid, PlaySite playSite) {
		return domainService.addKidToPlaySite(kid, playSite);
	}

	@Override
	public void removeKidFromPlaySite(Kid kid, PlaySite playSite) {
		domainService.removeKidFromPlaySite(kid, playSite);
	}

	@Override
	public void dequeueKidFromPlaySite(Kid kid, PlaySite playSite) {
		domainService.dequeueKidFromPlaySite(kid, playSite);
	}

	@Override
	public HashMap<PlaySite, List<Snapshot>> getAllPlaySiteUtilizationSnapshots() {
		return domainService.getAllPlaySiteUtilizationSnapshots();
	}

	@Override
	public List<PlaySiteUsageHistory> getAllPlaySiteUsageHistory() {
		return domainService.getAllPlaySiteUsageHistory();
	}

	@Override
	public List<PlaySiteVisitHistory> getAllPlaySiteVisitHistory() {
		return domainService.getAllPlaySiteVisitHistory();
	}
	
}
